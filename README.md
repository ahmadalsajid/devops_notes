# devops_notes

This repository will be used to keep track of learning DevOps 
things following https://roadmap.sh/devops

## Understanding Different OS Concepts
* [Process Management](os_concepts/process_management.md)
* [Threads and Concurrency](os_concepts/threads_concurrency.md)   
* [Sockets](os_concepts/sockets.md)
* [Posix Basics](os_concepts/posix.md)
* [Networking Concepts](os_concepts/networing_basic.md)
* [Startup Management(initd)](os_concepts/initd.md)
* [Service Management (systemd)](os_concepts/systemd.md)
* [I/O Management](os_concepts/io_management.md)
* [Virtualization](os_concepts/virtualization.md)
* [Memory/Storage](os_concepts/memory_storage.md)
* [File Systems](os_concepts/file_systems.md)

## Learn about managing servers
Get some administration knowledge in some OS. Go with any Linux distro, pick 
Ubuntu if you have a little no experience with Linux.  
#### Operating System
* [Ubuntu](managing_servers/os/ubuntu.md)
* [Linux](managing_servers/os/linux.md)
#### Terminal
* [Bash Scripting](managing_servers/terminal/bash_scripting.md)
* [Vim/nano](managing_servers/terminal/vim_nano.md)
* [Text Manipulation Tools](managing_servers/terminal/text_manipulation_tools.md)
* [Process Monitoring](managing_servers/terminal/process_monitoring.md)
* [Network](managing_servers/terminal/network.md)
* [System Performance](managing_servers/terminal/system_performance.md)
* [tmux](managing_servers/terminal/tmux.md)
* [Others](managing_servers/terminal/others.md)

## Networking, Security and Protocols
* [HTTP](networking_security_protocols/http.md)
* [HTTPS](networking_security_protocols/https.md)
* [FTP](networking_security_protocols/ftp.md)
* [SSL/TLS](networking_security_protocols/ssl_tls.md)
* [SSH](networking_security_protocols/ssh.md)
* [Port Forwarding](networking_security_protocols/port_forwarding.md)
* [Email](networking_security_protocols/email.md)

## What is and how to setup a
* [Reverse Proxy](setup/reverse_proxy.md)
* [Forward Proxy](setup/forward_proxy.md)
* [Caching Server](setup/caching_Server.md)
* [Load Balancer](setup/load_balancer.md)
* [Firewall](setup/firewall.md)
* [Web Server](setup/nginx.md)
    * [Nginx](setup/nginx.md)

## Infrastructure as Code (IaC)
* Containers
    * [Docker](iac/docker.md)
* Configuration Management
    * [Ansible](iac/ansible.md)
    * [Vagrant](iac/vagrant.md)
    * [Salt](iac/salt.md)
* Container Orchestration
    * [Kubernetes](iac/kubernetes.md)
* Infrastructure Provisioning
    * [CloudFormation](iac/cloudformation.md)
    * [Terraform](iac/terraform.md)
* Service Mesh
    * [Consul](iac/consul.md)
    * [Istio](iac/istio.md)
    
## CI/CD Tools
* [CircleCI](ci_cd/circleci.md)
* [Jenkins](ci_cd/jenkins.md)
* [Gitlab CI](ci_cd/gitlab_ci.md)
* [GitHub Actions](ci_cd/github_actions.md)

## Learn to monitor software and infrastructure
* Infrastructure Monitoring
    * [Prometheus](monitoring/prometheus.md)
    * [Grafana](monitoring/grafana.md)
* Application Monitoring
    * [Jaeger](monitoring/jaeger.md)
    * [New Relic](monitoring/new_relic.md)
* Logs Management
    * [Elastic](monitoring/elastic.md)

## Cloud Provides
* AWS
    * [EC2](cloud/aws/ec2.md)
    * [Lambda](cloud/aws/lambda.md)
    * [S3](cloud/aws/s3.md)
    
## Useful scripts
You can find some useful scripts in [this](scripts) section    

    